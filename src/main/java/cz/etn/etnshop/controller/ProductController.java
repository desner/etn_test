package cz.etn.etnshop.controller;

import cz.etn.etnshop.controller.helpers.BootstrapAlertBuilder;
import cz.etn.etnshop.controller.helpers.BootstrapAlert;
import cz.etn.etnshop.dao.Product;
import cz.etn.etnshop.dao.ProductDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cz.etn.etnshop.service.ProductService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@RequestParam(value = "search", required = false) String searchString) {

        ModelAndView modelAndView = new ModelAndView("product/list");
        modelAndView.addObject("count", productService.getProducts().size());

        if (searchString == null || searchString.equals("")) {
            modelAndView.addObject("products", productService.getProducts());
        } else {
//            try {
//                productService.indexProducts();
//            } catch (Exception ex) {
//                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            modelAndView.addObject("products", productService.findProducts(searchString));
        }

        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestParam("name") String name, @RequestParam("serial_nr") String serial_nr, RedirectAttributes redirectAttributes) {
        Product newProduct = new Product();
        newProduct.setName(name);
        newProduct.setSerial_nr(serial_nr);
        productService.saveProduct(newProduct);
        //Add info message
        BootstrapAlert alert = BootstrapAlertBuilder.success("Product added successfuly.");
        redirectAttributes.addFlashAttribute("alert", alert);
        return "redirect:/product/list";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String remove(@RequestParam("id") int id, RedirectAttributes redirectAttributes) {
        productService.deleteProduct(id);

        BootstrapAlert alert = BootstrapAlertBuilder.success("Product deleted successfuly.");
        redirectAttributes.addFlashAttribute("alert", alert);

        return "redirect:/product/list";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestParam("id") int id, @RequestParam("name") String name, @RequestParam("serial_nr") String serial_nr,
            RedirectAttributes redirectAttributes) {
        Product newProduct = new Product();
        newProduct.setName(name);
        newProduct.setSerial_nr(serial_nr);
        newProduct.setId(id);
        productService.updateProduct(newProduct);
        //Add info message
        BootstrapAlert alert = BootstrapAlertBuilder.success("Product updated successfuly.");
        redirectAttributes.addFlashAttribute("alert", alert);
        return "redirect:/product/list";
    }
}
