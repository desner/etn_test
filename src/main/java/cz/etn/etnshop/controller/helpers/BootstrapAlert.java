/*
 */
package cz.etn.etnshop.controller.helpers;

/**
 *
 * @author David Esner <esnerda at gmail.com>
 * @created 2016
 */
public class BootstrapAlert {

    public final String message;
    public final String style;

    public BootstrapAlert(String message, String alertStyle) {
        this.message = message;
        this.style = alertStyle;
    }

    public String getMessage() {
        return message;
    }

    public String getStyle() {
        return style;
    }

}
