/*
 */
package cz.etn.etnshop.controller.helpers;

/**
 *
 * @author David Esner <esnerda at gmail.com>
 * @created 2016
 */
public class BootstrapAlertBuilder {

    public static BootstrapAlert success(String message) {
        return new BootstrapAlert(message, "success");
    }

    public static BootstrapAlert error(String message) {
        return new BootstrapAlert(message, "error");
    }

    public static BootstrapAlert warning(String message) {
        return new BootstrapAlert(message, "warning");
    }

}
