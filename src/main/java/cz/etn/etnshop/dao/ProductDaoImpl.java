package cz.etn.etnshop.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {

    @Override
    public void indexProducts() {
        try {
            FullTextSession fullTextSession = Search.getFullTextSession(getSession());
            fullTextSession.createIndexer().startAndWait();
        } catch (Exception e) {
            //do nothing
        }
    }

    @Override
    public void saveProduct(Product product) {
        persist(product);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> getProducts() {
        Criteria criteria = getSession().createCriteria(Product.class);
        return (List<Product>) criteria.list();
    }

    @Override
    public void deleteProduct(int productId) {
        Query query = getSession().createSQLQuery("delete from Product where id = :id");
        query.setInteger("id", productId);
        query.executeUpdate();
    }

    @Override
    public void updateProduct(Product product) {
        getSession().update(product);

    }

    /**
     * Perform fultext search on all product fields.
     *
     * @param searchText
     * @return
     */
    @Override
    public List<Product> findProduct(String searchText) {
        FullTextSession fullTextSession = Search.getFullTextSession(getSession());

        QueryBuilder qb = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(Product.class).get();
        org.apache.lucene.search.Query query = qb
                .keyword().fuzzy().withThreshold(.4f).onFields("name", "serial_nr")
                .matching(searchText)
                .createQuery();

        org.hibernate.Query hibQuery
                = fullTextSession.createFullTextQuery(query, Product.class);

        List<Product> products = hibQuery.list();
        return products;
    }

}
