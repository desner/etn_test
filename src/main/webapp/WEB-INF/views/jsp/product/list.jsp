<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>etnShop</title>

        <spring:url value="/resources/core/css/hello.css" var="coreCss" />
        <spring:url value="/resources/core/css/bootstrap.min.css"
                    var="bootstrapCss" />
        <link href="${bootstrapCss}" rel="stylesheet" />
        <link href="${coreCss}" rel="stylesheet" />
        <script>var ctx = "${pageContext.request.contextPath}"</script>
    </head>

    <div class="container">

        <h2 class="">Products</h2>
        <c:if test="${not empty alert}">
            <div id="alertMsg">
                <div class="col-lg-12 alert alert-${alert.style} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>${alert.message}</div>

            </div>
        </c:if>
        <!-- Search bar -->
        <div class="input-group col-md-4 pull-left">
            <form action="./list" method="get" id="searchForm">
                <input type="text" class="form-control" name="search" id="productSearchText" placeholder="Find products">
            </form>
            <span class="input-group-btn">
                <button type="submit" form="searchForm" class="btn btn-default" id="searchProduct">Search</button>
            </span>

        </div>
        <!-- Add product btn -->
        <button type="button" class="pull-right btn btn-primary btn-md" data-toggle="modal" data-target="#addProductModal">
            + Add product
        </button>

        <table class="table">
            <thead>
                <tr>
                    <th class="col-md-1">ID</th>
                    <th class="col-md-4">Name</th>
                    <th class="col-md-3">Serial nr.</th>
                    <th class="col-md-2">Actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${products}" var="product">
                    <tr id="${product.id}">
                        <td>${product.id}</td>
                        <td class="prodName">${product.name}</td>
                        <td class="serialNr">${product.serial_nr}</td>
                        <td><a class="action edit"  title="Edit product"><span class="glyphicon glyphicon-pencil" aria-hidden="true">edit</span></a>
                            <a class="action delete" title="Delete product"><span class="glyphicon glyphicon-trash" aria-hidden="true">delete</span></a>
                        </td>
                    </tr>	
                </c:forEach>
            </tbody>
        </table>

        <hr>
        <footer>
            <p>&copy; Etnetera a.s. 2015</p>
        </footer>
    </div>


    <!--    Edit product modal dialog-->
    <div id="editProductModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Edit Product" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">                       
                    <h4 class="modal-title" id="gridSystemModalLabel">Edit product</h4>
                </div>
                <div class="modal-body">   
                    <form id="editProductFrm" action="" method="post">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="">Name</th>
                                    <th>Serial Nr.</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" name="name" required="true"></td>
                                    <td><input type="text" name="serial_nr" value=""></td>
                                    <td class="hidden"><input type="hidden" name="id" value=""></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="modal-footer">     
                    <button type="button" class="btn btn-default" data-dismiss="modal">Dismiss</button>
                    <button type="submit" form="editProductFrm" id="updateProduct" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!--Add product modal dialog-->
    <div id="addProductModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Create product" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">   
                    <h4 class="modal-title" id="prodFormTitle">Create new product</h4>
                </div>
                <div class="modal-body">
                    <form id="newProductFrm" action="" method="post">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="">Name</th>
                                    <th>Serial Nr.</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td><input type="text" name="name" required="true"></td>
                                    <td><input type="text" name="serial_nr" value=""></td>                                 
                                </tr>

                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="modal-footer"> 
                    <button type="button" class="btn btn-default" data-dismiss="modal">Dismiss</button>
                    <button type="submit" form="newProductFrm"  value="Submit" id="saveNewProduct" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <spring:url value="/resources/core/js/bootstrap.min.js"
                var="bootstrapJs" />
    <spring:url value="/resources/core/js/hello.js"
                var="appJs" />

    <script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${bootstrapJs}"></script>
    <script src="${appJs}"></script>

</body>
</html>