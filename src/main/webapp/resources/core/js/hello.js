//alert message
alertMsg = function () {};
alertMsg.success = function (message) {
    $('#alertMsg').html('<div class="col-lg-12 alert alert-success alert-dismissible" role="alert">\n\
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
};
//Actions with products
$('.edit').click(function () {//populate modal
    product = $(this).parent().parent();
    var prodName = product.find(".prodName").text();
    var prodSerial = product.find(".serialNr").text();
    prodId = $(this).parent().parent().attr("id");
    $('#editProductFrm').find("input[name=name]").val(prodName);
    $('#editProductFrm').find("input[name=serial_nr]").val(prodSerial);
    $('#editProductFrm').find("input[name=id]").val(prodId);
    $('#editProductModal').modal('show');
});
//delete
$('.delete').click(function () {//populate modal
    product = $(this).parent().parent();
    prodId = product.attr("id");
    //submit as a form, to let Spring handle the redirect
    $('<form action="' + ctx + '/product/remove" method="POST">' +
            '<input type="hidden" name="id" value="' + prodId + '">' +
            '</form>').submit();
});
//Update product 
$("#editProductFrm").on('submit', function (e) {
    var name = $(this).find('input[name=name]').val();
    if (name === null || name === "") {
        alert("Name must be filled out!");
        return false;
    }
    $('#editProductFrm').attr('action', ctx + "/product/update");
    return true;
});
//Add product
$("#newProductFrm").on('submit', function (e) {
    var name = $(this).find('input[name=name]').val();
    if (name === null || name === "") {
        alert("Name must be filled out!");
        return false;
    }

    $('#newProductFrm').attr('action', ctx + "/product/add");
    return true;
});
//search product fulltext



